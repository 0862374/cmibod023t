/*
 * Rob Weber 0854522
 * Decision Tree
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RobDecisionTree {


	// This method is called first and reads the .cvs file, converts it and calls the method that generates the decision trees
	public void decisionTree(String fileLocation) throws FileNotFoundException {
		Scanner csvTable = new Scanner(new File(fileLocation));									// Read .csv

		List<String> list = new ArrayList<String>();
		int columnCount = 0;

		while(csvTable.hasNext()) {																// Insert .csv data into a List
			csvTable.useDelimiter("\n");														// Split on new line
			String obj = csvTable.next();														// Get next row
			columnCount = (obj.length() - obj.replace(",", "").length()) + 1;					// Calculate column count
			list.addAll(Arrays.asList(obj.split(",")));											// Add all rows to list
		}
		csvTable.close();																		// Close scanned .csv object

		String[][] matrix = listToMatrix(list, columnCount);									// Convert to matrix<String> to do calculations with 
		
		decTree(matrix);
	}


	// Generates decision tree
	public void decTree(String[][] matrix) {
		System.out.println("Decision Tree:");
	}



	// Counts occurrences of "yes" and "no" in the target column, for a specific test value
	public double yesOrNoTestValueCount(boolean yes, String testValue, int columnNumber, String[][] matrix) {
		double count = 0;
		String compareString = "";
		int targetColumn = matrix[0].length - 1;

		if (yes) {																				// Check if "yes"- or "no"-count is requested
			compareString = "yes|Yes|YES";
		} else if (!yes) {
			compareString = "no|No|NO";
		}

		for (int index = 1; index < matrix.length; index++) {									// Checks if test value is is "yes" or "no" and counts how often
			if (matrix[index][columnNumber].equals(testValue) && matrix[index][targetColumn].matches(compareString)) {
				count++;
			}
		}

		return count;
	}


	// Counts occurrences of "yes" and "no" in the target column
	public double yesOrNoCount(boolean yes, String[][] matrix) {
		double count = 0;
		String compareString = "";
		int targetColumn = matrix[0].length - 1;

		if (yes) {																				// Check if "yes"- or "no"-count is requested
			compareString = "yes|Yes|YES";
		} else if (!yes) {
			compareString = "no|No|NO";
		}

		for (int index2 = 1; index2 < matrix.length; index2++) {								// Loop through target column
			if (matrix[index2][targetColumn].matches(compareString)) {							// Add 1 to count if index is equal to value in compare String
				count++;
			}
		}

		return count;
	}


	// Turn list into String[][] matrix
	public String[][] listToMatrix(List<String> list, int columnCount) {
		int rowCount = list.size() / columnCount;
		String[][] matrix = new String[rowCount][columnCount];

		int count = 0;
		for (int row = 0; row < rowCount; row++) {
			for (int column = 0; column < columnCount; column++) {
				matrix[row][column] = list.get(count);
				count++;
			}
		}
		return matrix;
	}


	// Print array
	public void printArray(Object[] array) {
		for (int index = 0; index < array.length; index++) {
			System.out.print(array[index] + " ");
		}
		System.out.println();
	}
}