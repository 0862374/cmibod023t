package Model;

import java.util.HashMap;
import java.util.Map;

public class Row {
	public Map<String, String> columnValues = new HashMap<String, String>();
	
	public Row(String[] keys, String[] values){
		for(int i = 0; i < keys.length; i++){
			columnValues.put(keys[i], values[i]);
		}
	}
	
	public boolean getCombination(String key, String value, String keyOne, String valueOne, String keyTwo, String valueTwo){
		if(columnValues.containsKey(key) && columnValues.containsKey(keyOne) && columnValues.containsKey(keyOne)){
			if(columnValues.get(key).equals(value) && columnValues.get(keyOne).equals(valueOne) && columnValues.get(keyTwo).equals(valueTwo)){
				return true;
			}
		}
		
		return false;
	}
}
