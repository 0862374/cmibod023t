package Model;

import java.util.ArrayList;
import java.util.List;

public class HeaderEntry {
	public String headerName;
	public List<Column> columnValues = new ArrayList<Column>();
	
	public int error = 0;
	public int total = 0;

	public HeaderEntry(String headerName) {
		this.headerName = headerName;
	}
	
	public int columnIndex(String value){
		for(int i = 0; i < columnValues.size(); i++){
			if(columnValues.get(i).value.equals(value)){
				return i;
			}
		}
		return 0;
	}
	
	
}
