package Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Column {
	public String value;
	public Map<String, Integer> errors = new HashMap<String, Integer>();
	public List<Row> rows = new ArrayList<Row>();

	public Column(String value) {
		this.value = value;
	}
	
	public void addToRows(String[] keys, String[] values){
		rows.add(new Row(keys, values));
		
	}

	public void addPoint(String error) {
		if (errors.containsKey(error)) {
			errors.put(error, (errors.get(error) + 1));
		} else {
			errors.put(error, 1);
		}
	}
}