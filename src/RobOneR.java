/*
 * Rob Weber 0854522
 * 
 * How this class works:
 * 1.	Import .csv into matrix<String>
 * 2. 	Per predictor column {
 * 			Per predictor value {
 * 				Create sub array of target values
 * 				Per target value {
 * 					Count occurrences per value
 * 				}
 * 				Calculate error
 * 			}
 * 			Calculate total error
 * 		}
 *		Calculate lowest total error
 * 3.	Print lowest error predictor name and value
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RobOneR {
	
	
	// This method is called first and reads the .cvs file, converts it and calls the method that calculates the lowest error
	public void oneR(String fileLocation, String target) throws FileNotFoundException {
    	Scanner csvTable = new Scanner(new File(fileLocation));									// Read .csv
    	
    	List<String> list = new ArrayList<String>();
    	int columnCount = 0;
    	
        while(csvTable.hasNext()) {																// Insert .csv data into a List
        	csvTable.useDelimiter("\n");														// Split on new line
        	String obj = csvTable.next();														// Get next row
        	columnCount = (obj.length() - obj.replace(",", "").length()) + 1;					// Calculate column count
        	list.addAll(Arrays.asList(obj.split(",")));											// Add all rows to list
        }
        csvTable.close();																		// Close scanned .csv object
        
        String[][] matrix = listToMatrix(list, columnCount);									// Convert to matrix<String> to do calculations with
        
        lowestError(matrix, target);															// Get predictor with lowest error and print on screen
	}
	
	
	// Calculates predictor with lowest error and print on screen
	public void lowestError(String[][] matrix, String target) {
		int targetColumnNumber = getTargetColumnNumber(matrix, target);
		int numberOfPredictorColumns = matrix[0].length - 1;
		
		double[] errors = new double[numberOfPredictorColumns];
		String[] predictors = new String[numberOfPredictorColumns];
		
		for (int index = 0; index < numberOfPredictorColumns; index++) {						// Loop through matrix
			errors[index] = calcError(getColumn(matrix, index), getColumn(matrix, targetColumnNumber));		// Calculate error for every predictor
			predictors[index] = matrix[0][index];
		}
		
		System.out.println("OneR:\nLowest error predictor is: " + predictors[indexOf(errors, findLowest(errors))]);	// Find lowest predictor error name and print
		System.out.println("With total error of: " + findLowest(errors) + "\n");				// Find lowest predictor error value and print
	}
	
	
	// Calculates error per predictor column
	public double calcError(String[] predictor, String[] target) {
		String[] unique = getUniqueValues(predictor);
		double error, popular, size, totalError, totalSize;
		error = popular = size = totalError = totalSize = 0;
		
		for (int index = 0; index < unique.length; index++) {
			String[] targetSub = targetSubstring(predictor, target, unique[index]);
			popular = Double.parseDouble(getPopular(targetSub)[1]);	
			size = targetSub.length;
			error = (size - popular);
			totalError = totalError + error;
			totalSize = totalSize + size;
		}
		totalError = totalError / totalSize;
		return totalError;
	}
	
	
	// Creates sub array<String> of only certain String values from the target
	public String[] targetSubstring(String[] predictor, String[] target, String value) {
		String[] result = new String[predictor.length];
		int count = 0;
		for (int index = 0; index < predictor.length; index++) {
			if (predictor[index].equals(value)) {
				result[count] = target[index];
				count++;
			}
		}
		return trim(result);
	}
	
	
	// Returns array<String> with unique values from an array<String>
	public String[] getUniqueValues(String[] array) {
		String compare;
		int count = 0;
		String[] result = new String[array.length];
		for (int index = 1; index < array.length; index++) {
			compare = array[index];
			if (!containsValue(result, compare)) {
				result[count] = compare;
				count++;
			}
		}
		return trim(result);
	}
	
	
	// Checks if array<String> contains certain value
	public boolean containsValue(String[] array, String value) {
		for (int index = 0; index < array.length; index++) {
			if (array[index] != null) {
				if (array[index].equals(value)) {
					return true;
				}
			}
		}
		return false;
	}
	
	
	// Removes null values from array<String>
	public String[] trim(String[] array) {
		int count = 0;
		for (int index = 0; index < array.length; index++) {
			if (array[index] != null) {
				count++;
			}
		}
		String[] result = new String[count];
		count = 0;
		for (int index = 0; index < array.length; index++) {
			if (array[index] != null) {
				result[count] = array[index];
				count++;
			}
		}
		return result;
	}
	
	
	// Finds lowest value in array<double>
	public double findLowest(double[] array) {
		if (array.length != 0) {
			double small = array[0];
			for (int index = 0; index < array.length; index++) {
				if (array[index] < small) {
					small = array[index];
				}
			}
			return small;
		} else {
			return 0;
		}
	}
	
	
	// Gets index of value in array<double>
	public int indexOf(double[] array, double value) {
		int returnIndex = 0;
		for (int index = 0; index < array.length; index++) {
			if (array[index] == value) {
				returnIndex = index;
			}
		}
		return returnIndex;
	}
	
	
	// Gets column from matrix
	public String[] getColumn(String[][] matrix, int columnNumber) {
		String[] column = new String[matrix.length];
		for (int index = 0; index < matrix.length; index++) {
			column[index] = matrix[index][columnNumber];
		}
		return column;
	}
	
	
	// Gets target column number
	public int getTargetColumnNumber(String[][] matrix, String target) {
		int targetColumnNumber = 0;
		for (int index = 0; index < matrix[0].length; index++) {
			if (matrix[0][index].equals(target)) {
				targetColumnNumber = index;
			}
		}
		return targetColumnNumber;
	}
		
	
	// Turns list into matrix
	public String[][] listToMatrix(List<String> list, int columnCount) {
		int rowCount = list.size() / columnCount;
		String[][] matrix = new String[rowCount][columnCount];
		
		int count = 0;
		for (int row = 0; row < rowCount; row++) {
			for (int column = 0; column < columnCount; column++) {
				matrix[row][column] = list.get(count);
				count++;
			}
		}
		return matrix;
	}
	
	
	// Loops through Array and returns most popular value with count
	public String[] getPopular(String[] array) {
		int count = 0;
		int tempCount = 0;
		String popular = array[0];
		String temp = "";
		
		for (int index = 0; index < array.length; index++) {
			temp = array[index];
			tempCount = 0;
			for (int index2 = 0; index2 < array.length; index2++) {
				if (temp.equals(array[index2])) {
					tempCount++;
				}
			}
			if (tempCount > count) {
				popular = temp;
				count = tempCount;
			}
		}
		String[] returnString = new String[2];
		returnString[0] = popular;
		returnString[1] = Integer.toString(count);
		return returnString;
	}
	
	
	// Prints matrix
	public void printMatrix(String matrix[][]) {
		for (int row = 0; row < matrix.length; row++) {
			System.out.print("| ");
			for (int column = 0; column < matrix[0].length; column++) {
				System.out.print(matrix[row][column] + " | ");
			}
			System.out.print("\n");
		}
	}
	
	
	// Prints array
	public void printArray(Object[] array) {
		for (int index = 0; index < array.length; index++) {
			System.out.print(array[index] + " ");
		}
		System.out.println();
	}
}