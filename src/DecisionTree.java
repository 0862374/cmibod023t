import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import Model.Column;
import Model.HeaderEntry;

public class DecisionTree {
	List<HeaderEntry> headEntries;
	List<HeaderEntry> alreadyCalculated = new ArrayList<HeaderEntry>();
	String fileToParse;
	OneR oner;

	public DecisionTree(String fileToParse) {
		this.fileToParse = fileToParse;
		oner = new OneR(fileToParse);
		headEntries = oner.calculateErrors();
		
		calculateTree(getRoot(headEntries), headEntries, null, null);
		
	}

	public double entrophy(int p, int q) {
		double pq = p / ((double) p + (double) q);
		double qp = q / ((double) p + (double) q);

		double ans = 0;
		if (p > 0) {
			ans += (-pq * Math.log(pq));
		}
		if (q > 0) {
			ans += (-qp * Math.log(qp));
		}
		// System.out.println(ans / Math.log(2));

		return (ans / Math.log(2));
	}

	public double entrophy(int p, int q, int r) {
		double pq = p / ((double) p + (double) q + (double) r);
		double qp = q / ((double) p + (double) q + (double) r);
		double qr = r / ((double) p + (double) q + (double) r);

		double ans = 0;
		if (p > 0) {
			ans += (-pq * Math.log(pq));
		}
		if (q > 0) {
			ans += (-qp * Math.log(qp));
		}
		if (r > 0) {
			ans += (-qr * Math.log(qr));
		}
		// System.out.println(ans / Math.log(2));

		return (ans / Math.log(2));
	}

	public HeaderEntry getRoot(List<HeaderEntry> headEntries) {
		HeaderEntry headerentry = null;
		HeaderEntry lastEntry = getHeaderEntryWithErrors(new OneR(fileToParse)
				.GetLastHeadEntry());
		Map<HeaderEntry, Double> gainRatio = new HashMap<HeaderEntry, Double>();

		for (HeaderEntry head : headEntries) {

			gainRatio.put(head, (double) 0);

			for (Column col : head.columnValues) {

				double g = (((double) OneR.getTotal(col.errors) / (double) lastEntry.total) * entrophy(
						getInt(col.errors.values().toArray(), 1),
						getInt(col.errors.values().toArray(), 0)));

				gainRatio.put(head, (gainRatio.get(head) + g));
			}

			double splitInfo = 0;
			if (head.columnValues.size() == 3) {
				splitInfo = entrophy(
						OneR.getTotal(head.columnValues.get(0).errors),
						OneR.getTotal(head.columnValues.get(1).errors),
						OneR.getTotal(head.columnValues.get(2).errors));
			} else {
				splitInfo = entrophy(
						OneR.getTotal(head.columnValues.get(0).errors),
						OneR.getTotal(head.columnValues.get(1).errors));
			}

			gainRatio.put(
					head,
					((entrophy(lastEntry.error,
							(lastEntry.total - lastEntry.error)) - gainRatio
							.get(head)) / splitInfo));

		}

		double GreatestGainRatio = 0;

		for (Iterator<Entry<HeaderEntry, Double>> error = gainRatio.entrySet()
				.iterator(); error.hasNext();) {
			Entry<HeaderEntry, Double> entry = error.next();

			if (entry.getValue() > GreatestGainRatio) {
				headerentry = entry.getKey();
				GreatestGainRatio = entry.getValue();
			}
		}

		alreadyCalculated.add(headerentry);
		return headerentry;

	}

	public void calculateTree(HeaderEntry headerRoot, List<HeaderEntry> headerEntries, HeaderEntry headerNode, Column columnNode){
		HeaderEntry lastHeader = new OneR(fileToParse).GetLastHeadEntry();
		alreadyCalculated.add(headerRoot);
		
		for(Column column : headerRoot.columnValues){
			HeaderEntry HighestHeaderEntry = null;
			double highestGainRatio = 0;
			
			for(HeaderEntry head : headerEntries){
				
				if(!headerRoot.headerName.equals(head.headerName) && !contains(head)){
					int headYes;
					int headNo;
					if(headerNode == null){
					headYes = counter(column, headerRoot.headerName, column.value, headerRoot.headerName, column.value, lastHeader.headerName, lastHeader.columnValues.get(1).value);
					headNo = counter(column, headerRoot.headerName, column.value, headerRoot.headerName, column.value, lastHeader.headerName, lastHeader.columnValues.get(0).value);
					} else {
						headYes = counter(column, headerRoot.headerName, column.value, headerNode.headerName, columnNode.value, lastHeader.headerName, lastHeader.columnValues.get(1).value);
						headNo = counter(column, headerRoot.headerName, column.value, headerNode.headerName, columnNode.value, lastHeader.headerName, lastHeader.columnValues.get(0).value);
						
					}
					double infoHeader = entrophy(headYes, headNo);
					double tempInfo = 0;
					double headSplit = 0;
					double gainRatio = 0;
					
					List<Integer> totalYesNoCol = new ArrayList<>();
					
					for(Column col : head.columnValues){
						int colYes;
						int colNo;
						if(headerNode == null){
						colYes = counter(col, headerRoot.headerName, column.value, headerRoot.headerName, column.value, lastHeader.headerName, lastHeader.columnValues.get(1).value);
						colNo = counter(col, headerRoot.headerName, column.value, headerRoot.headerName, column.value, lastHeader.headerName, lastHeader.columnValues.get(0).value);
						} else {
							colYes = counter(col, headerRoot.headerName, column.value, headerNode.headerName, columnNode.value, lastHeader.headerName, lastHeader.columnValues.get(1).value);
							colNo = counter(col, headerRoot.headerName, column.value, headerNode.headerName, columnNode.value, lastHeader.headerName, lastHeader.columnValues.get(0).value);
							
						}
						
						tempInfo += ((colYes + colNo) * entrophy(colYes, colNo));
						totalYesNoCol.add(colYes + colNo);
					}
					
					double headGain = (infoHeader - (tempInfo) / (headYes + headNo));
										
					if(totalYesNoCol.size() == 2){
						headSplit = entrophy(totalYesNoCol.get(0), totalYesNoCol.get(1));
					} else {
						headSplit = entrophy(totalYesNoCol.get(0), totalYesNoCol.get(1), totalYesNoCol.get(2));
					}
					
					gainRatio = (headGain / headSplit);
										
					if(gainRatio > highestGainRatio){
						highestGainRatio = gainRatio;
						HighestHeaderEntry = head;
					}
					
				}
								
			}
			
			if(highestGainRatio != 0){
								
				if(!contains(HighestHeaderEntry)){
					System.out.println(headerRoot.headerName + ":" + column.value + "->" + HighestHeaderEntry.headerName);
					calculateTree(HighestHeaderEntry, headerEntries, headerRoot, column);
					System.out.println();
					
				} else {
					int colYes;
					int colNo;
					if(headerNode == null){
					colYes = counter(column, headerRoot.headerName, column.value, headerRoot.headerName, column.value, lastHeader.headerName, lastHeader.columnValues.get(1).value);
					colNo = counter(column, headerRoot.headerName, column.value, headerRoot.headerName, column.value, lastHeader.headerName, lastHeader.columnValues.get(0).value);
					} else {
						colYes = counter(column, headerRoot.headerName, column.value, headerNode.headerName, columnNode.value, lastHeader.headerName, lastHeader.columnValues.get(1).value);
						colNo = counter(column, headerRoot.headerName, column.value, headerNode.headerName, columnNode.value, lastHeader.headerName, lastHeader.columnValues.get(0).value);
						
					}				
					if(colYes >= colNo){
						System.out.println(headerRoot.headerName + ":" + column.value + "--->" + lastHeader.columnValues.get(1).value);
					} else {
						System.out.println(headerRoot.headerName + ":" + column.value + "--->" + lastHeader.columnValues.get(0).value);
					}
					
				}
			} else {
				if(headerNode != null){
					
					int	colYes = counter(column, headerRoot.headerName, column.value, headerNode.headerName, columnNode.value, lastHeader.headerName, lastHeader.columnValues.get(1).value);
					int	colNo = counter(column, headerRoot.headerName, column.value, headerNode.headerName, columnNode.value, lastHeader.headerName, lastHeader.columnValues.get(0).value);
						
					if(colYes >= colNo){
						System.out.println(headerRoot.headerName + ":" + column.value + "--->" + lastHeader.columnValues.get(1).value);
					} else {
						System.out.println(headerRoot.headerName + ":" + column.value + "--->" + lastHeader.columnValues.get(0).value);
					}
				} else {
					int colYes = counter(column, headerRoot.headerName, column.value, headerRoot.headerName, column.value, lastHeader.headerName, lastHeader.columnValues.get(1).value);
					int colNo = counter(column, headerRoot.headerName, column.value, headerRoot.headerName, column.value, lastHeader.headerName, lastHeader.columnValues.get(0).value);					
					
					if(colYes >= colNo){
						System.out.println(headerRoot.headerName + ":" + column.value + "--->" + lastHeader.columnValues.get(1).value);
					} else {
						System.out.println(headerRoot.headerName + ":" + column.value + "--->" + lastHeader.columnValues.get(0).value);
					}
					
					System.out.println();
					
				}
			}
		}
	}

	public int getInt(Object[] array, int place) {
		if (array.length <= place) {
			return 0;
		}

		return (int) array[place];
	}

	public HeaderEntry getHeaderEntryWithErrors(HeaderEntry headerentry) {
		List<Integer> errors = new ArrayList<Integer>();

		for (Column col : headerentry.columnValues) {
			headerentry.error = (headerentry.error + (OneR.getTotal(col.errors) - col.errors
					.get(OneR.getKeyMostErrors(col.errors))));

			errors.add(col.errors.get(OneR.getKeyMostErrors(col.errors)));

			headerentry.total = (headerentry.total + OneR.getTotal(col.errors));
		}

		for (int a : errors) {
			if (a > headerentry.error) {
				headerentry.error = a;
			}
		}

		headerentry.error = (headerentry.total - headerentry.error);

		return headerentry;
	}

	public int counter(Column col, String key, String value, String keyOne,
			String valueOne, String keyTwo, String valueTwo) {
		int cnt = 0;

		for (int i = 0; i < col.rows.size(); i++) {
			if (col.rows.get(i).getCombination(key, value, keyOne, valueOne,
					keyTwo, valueTwo)) {
				cnt++;

			}
		}

		return cnt;
	}
	
	public boolean contains(HeaderEntry head){
		
		
		for(HeaderEntry header : alreadyCalculated){
			if(header.headerName.equals(head.headerName)){
				return true;
			}
		}
		return false;
	}

}
