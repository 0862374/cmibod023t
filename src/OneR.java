import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import Model.Column;
import Model.HeaderEntry;
import Model.Row;

public class OneR {
	BufferedReader fileReader;

	public OneR(String fileToParse) {

		readFile(fileToParse);

	}

	private void readFile(String fileToParse) {

		try {
			fileReader = new BufferedReader(new FileReader(fileToParse));

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public List<HeaderEntry> calculateErrors() {
		try {

			String[] columns;
			String line;
			int totalColumns = 0;
			

			columns = fileReader.readLine().split(",");
			String[] headerColumns = columns;
			totalColumns = columns.length;

			List<HeaderEntry> headerEntries = new ArrayList<HeaderEntry>();

			for (int i = 0; i < (totalColumns - 1); i++) {
				headerEntries.add(new HeaderEntry(columns[i]));
			}
			
			

			while ((line = fileReader.readLine()) != null) {
				columns = line.split(",");

				for (int i = 0; i < (totalColumns - 1); i++) {
					if (!containsInArray(columns[i],
							headerEntries.get(i).columnValues)) {
						headerEntries.get(i).columnValues.add(new Column(
								columns[i]));
												
					}

					for (Column col : headerEntries.get(i).columnValues) {
						if (col.value.equals(columns[i])) {
							col.addPoint(columns[(totalColumns - 1)]);
						}
					}
					
					headerEntries.get(i).columnValues.get(headerEntries.get(i).columnIndex(columns[i])).addToRows(headerColumns, columns);
				}
				
				

			}

			for (HeaderEntry head : headerEntries) {

				for (Column col : head.columnValues) {
					head.error = (head.error + (getTotal(col.errors) - col.errors
							.get(getKeyMostErrors(col.errors))));
					head.total = (head.total + getTotal(col.errors));
/*
					System.out.println(head.headerName
							+ ":"
							+ col.value
							+ "->"
							+ getKeyMostErrors(col.errors)
							+ ":"
							+ (getTotal(col.errors) - col.errors
									.get(getKeyMostErrors(col.errors))) + "/"
							+ getTotal(col.errors));*/

				}

				/*System.out.println("Totale fout: " + head.error + "/"
						+ head.total);
				System.out
						.println("------------------------------------------------------");*/
			}

			return headerEntries;

		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public HeaderEntry GetLastHeadEntry() {
		try {

			String[] columns;
			String line;
			int totalColumns = 0;

			columns = fileReader.readLine().split(",");
			totalColumns = columns.length;

			HeaderEntry headEntry = new HeaderEntry(columns[totalColumns - 1]);

			while ((line = fileReader.readLine()) != null) {
				columns = line.split(",");

				int i = (totalColumns - 1);

				if (!containsInArray(columns[i], headEntry.columnValues)) {
					headEntry.columnValues.add(new Column(columns[i]));
				}

				for (Column col : headEntry.columnValues) {
					if (col.value.equals(columns[i])) {
						col.addPoint(columns[(totalColumns - 1)]);
					}
				}
			}

			return headEntry;

		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public List<HeaderEntry> getLowestErrors() {
		List<HeaderEntry> headerEntries = calculateErrors();
		List<HeaderEntry> lowestErrors = new ArrayList<HeaderEntry>();

		for (HeaderEntry headerEntry : headerEntries) {
			if (lowestErrors.size() <= 0) {
				lowestErrors.add(headerEntry);
			} else {
				if (headerEntry.error < lowestErrors.get(0).error) {
					lowestErrors.clear();
					lowestErrors.add(headerEntry);
				} else if (headerEntry.error == lowestErrors.get(0).error) {
					lowestErrors.add(headerEntry);
				}
			}
		}

		for (HeaderEntry headerEntry : lowestErrors) {
			System.out.println("Lowest error: " + headerEntry.headerName + ":"
					+ (headerEntry.total - headerEntry.error) + "/"
					+ headerEntry.total);
		}

		return lowestErrors;
	}

	public static String getKeyMostErrors(Map<String, Integer> errors) {
		String errorKey = null;
		int count = 0;
		for (Iterator<Entry<String, Integer>> error = errors.entrySet()
				.iterator(); error.hasNext();) {
			Entry<String, Integer> entry = error.next();

			if (entry.getValue() > count) {
				errorKey = entry.getKey();
				count = entry.getValue();
			}
		}

		return errorKey;
	}

	public static int getTotal(Map<String, Integer> errors) {
		int count = 0;

		for (Iterator<Entry<String, Integer>> error = errors.entrySet()
				.iterator(); error.hasNext();) {
			Entry<String, Integer> entry = error.next();
			count = (count + entry.getValue());
		}
		return count;
	}

	private boolean containsInArray(String item, List<Column> array) {
		if (array.size() == 0) {
			return false;
		}

		for (Column ite : array) {
			if (item.equals(ite.value))
				return true;
		}

		return false;
	}
	
}
