// Rob Weber 0854522
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RobZeroR {
	
	public void zeroR(String fileLocation, String target) throws FileNotFoundException {
		
    	Scanner scanner = new Scanner(new File(fileLocation));			// Read .csv
    	List<String> list = new ArrayList<String>();
    	int columnCount = 0;
        
        while(scanner.hasNext()) {										// Insert .csv data into a List
        	scanner.useDelimiter("\n");
        	String obj = scanner.next();
        	columnCount = (obj.length() - obj.replace(",", "").length()) + 1;
        	list.addAll(Arrays.asList(obj.split(",")));
        }
        scanner.close();
        
        int rowCount = list.size()/columnCount;
        String targetColumn[] = new String[rowCount];
        
        for (int i = 0; i < rowCount; i++) {							// Create target column Array
        	targetColumn[i] = list.get(list.indexOf(target) + (i * columnCount));
        }
        
        String popularValue = getPopular(targetColumn)[0];				// Get most popular value in target column
        double popularCount = Double.parseDouble(getPopular(targetColumn)[1]);		// Get count of most popular value
        
        System.out.println("ZeroR:\n" + target + " = " + popularValue + "' with accuracy of " + (popularCount / (rowCount-1)) + "\n");
	}
	
	public String[] getPopular(String[] array) {						// Loops through Array and returns most popular value with count
		int count = 1;
		int tempCount = 0;
		String popular = array[1];
		String temp = "";
		
		for (int i = 0; i < (array.length - 1); i++) {
			temp = array[i];
			tempCount = 0;
			for (int j = 1; j < array.length; j++) {
				if (temp.equals(array[j])) {
					tempCount++;
				}
			}
			if (tempCount > count) {
				popular = temp;
				count = tempCount;
			}
		}
		
		String[] returnString = new String[2];
		returnString[0] = popular;
		returnString[1] = Integer.toString(count);
		return returnString;
	}
}