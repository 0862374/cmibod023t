import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

import Model.Column;
import Model.HeaderEntry;

public class StatisticalModeling {
	BufferedReader fileReader;
	String fileToParse;
	List<HeaderEntry> headEntries;
	List<JComboBox<String>> comboBoxes = new ArrayList<JComboBox<String>>();
	JFrame frame;
	JLabel answerLbl = null;

	public StatisticalModeling(String fileToParse) {
		readFile(fileToParse);
		this.fileToParse = fileToParse;
	}

	private void readFile(String fileToParse) {

		try {
			fileReader = new BufferedReader(new FileReader(fileToParse));

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void createScreen() {
		frame = new JFrame("Statistical Modeling");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		headEntries = new OneR(fileToParse).calculateErrors();

		int i = 40;
		for (HeaderEntry headentry : headEntries) {

			List<String> values = new ArrayList<String>();
			values.add(headentry.headerName);

			for (Column col : headentry.columnValues) {
				values.add(col.value);
			}

			JComboBox<String> box = new JComboBox<String>(
					values.toArray(new String[values.size()]));

			box.setLocation(i, 50);
			box.setName(headentry.headerName);
			box.setSize(box.getPreferredSize());
			frame.add(box);

			i = (i + box.getPreferredSize().width);
			comboBoxes.add(box);
		}

		JButton btn = new JButton();
		btn.setText("Calculate");
		btn.setVisible(true);
		btn.setSize(btn.getPreferredSize());
		btn.setLocation(i, 50);
		frame.add(btn);

		btn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				calculate();
			}
		});

		answerLbl = new JLabel();
		frame.add(answerLbl);

		frame.setSize(800, 500);
		frame.setVisible(true);
		frame.repaint();
	}

	private void calculate() {
		int length = comboBoxes.size();
		String answerTxt = "";
		HeaderEntry headEntry = new OneR(fileToParse).GetLastHeadEntry();

		for (Column col : headEntry.columnValues) {
			if(answerTxt.length() != 0) {answerTxt += "<br>";}
			answerTxt += col.value + "->";
			
			float answer = 1;
			for (int i = 0; i < length; i++) {
				if (!headEntries.get(i).headerName.equals(comboBoxes.get(i)
						.getSelectedItem())) {
					
					if (answer != 1){
						answerTxt += " * ";}
					answerTxt += (float) ((float) getErrors(
							getColumnByValue(headEntries.get(i).columnValues,
									comboBoxes.get(i).getSelectedItem()
											.toString()).errors, col.value) / (float) getTotal(
							headEntries.get(i).columnValues, col.value));
					
					answer = answer
							* (float) ((float) getErrors(
									getColumnByValue(
											headEntries.get(i).columnValues,
											comboBoxes.get(i).getSelectedItem()
													.toString()).errors, col.value) / (float) getTotal(
									headEntries.get(i).columnValues, col.value));
					
				}
			}
			
			answerTxt += " = " + answer;
			
		}
		answerLbl.setText("<html>" + answerTxt + "</html>");
	}

	private int getErrors(Map<String, Integer> errors, String searchError) {
		int count = 0;

		for (Iterator<Entry<String, Integer>> error = errors.entrySet()
				.iterator(); error.hasNext();) {
			Entry<String, Integer> entry = error.next();

			if (entry.getKey().equals(searchError)) {
				count = (count + entry.getValue());
			}
		}

		return count;
	}

	private int getTotal(List<Column> columns, String searchError) {
		int count = 0;

		for (Column col : columns) {
			count = (count + getErrors(col.errors, searchError));
		}

		return count;
	}

	public Column getColumnByValue(List<Column> columns, String value) {

		for (Column col : columns) {
			if (col.value.equals(value)) {
				return col;
			}
		}

		return null;
	}

}
