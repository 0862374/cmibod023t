import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ZeroR {
	BufferedReader fileReader;
	int columnCount;
	int rowCount;

	boolean hasHeader;

	public ZeroR(String fileToParse, boolean hasHeader) {
		readFile(fileToParse);
		this.hasHeader = hasHeader;
	}

	public void readFile(String fileToParse) {

		try {
			fileReader = new BufferedReader(new FileReader(fileToParse));

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void printFrequention() {
		List<Pair> pairs = getFrequention();

		for (Pair pair : pairs) {
			System.out.println(pair.text + " : "
					+ ((float) pair.count / (float) rowCount) + " : "
					+ pair.count);
		}
	}

	// The last column is always the success/failure column
	public List<Pair> getFrequention() {
		String[] tokens;
		List<Pair> succesValues = new ArrayList<Pair>();

		try {
			String line;
			tokens = fileReader.readLine().split(",");
			columnCount = tokens.length;

			int i = 0;
			if (!hasHeader) {
				if (!containsString(tokens[columnCount - 1], succesValues)) {
					succesValues.add(new Pair(tokens[columnCount - 1]));
				}

				addPoint(tokens[columnCount - 1], succesValues);

				i++;
			}

			while ((line = fileReader.readLine()) != null) {

				tokens = line.split(",");

				if (!containsString(tokens[columnCount - 1], succesValues)) {
					succesValues.add(new Pair(tokens[columnCount - 1]));
				}

				addPoint(tokens[columnCount - 1], succesValues);

				i++;
			}

			rowCount = i;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// return (succesCount / rowCount);
		return succesValues;
	}

	public static boolean containsString(String string, List<Pair> array) {

		for (int i = 0; i < array.size(); i++) {
			if (string.contains(array.get(i).text)) {
				return true;
			}
		}
		return false;
	}

	public void addPoint(String string, List<Pair> array) {
		for (int i = 0; i < array.size(); i++) {
			if (string.contains(array.get(i).text)) {
				array.get(i).count++;
			}
		}
	}

	public class Pair {
		public String text;
		public int count = 0;

		public Pair(String text) {
			this.text = text;
		}
	}

}
