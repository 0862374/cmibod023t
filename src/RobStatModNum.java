/*
 * Rob Weber 0854522
 * Statistical Modeling Numeric, using Naive Bayes and Laplace Estimator
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RobStatModNum {


	// This method is called first and reads the .cvs file, converts it and calls the method that calculates likelihoods and probabilities
	public void statModNum(String fileLocation, String[] testValues) throws FileNotFoundException {
		Scanner csvTable = new Scanner(new File(fileLocation));									// Read .csv
		
		List<String> list = new ArrayList<String>();
		int columnCount = 0;

		while(csvTable.hasNext()) {																// Insert .csv data into a List
			csvTable.useDelimiter("\n");														// Split on new line
			String obj = csvTable.next();														// Get next row
			columnCount = (obj.length() - obj.replace(",", "").length()) + 1;					// Calculate column count
			list.addAll(Arrays.asList(obj.split(",")));											// Add all rows to list
		}
		csvTable.close();																		// Close scanned .csv object

		String[][] matrix = listToMatrix(list, columnCount);									// Convert to matrix<String> to do calculations with

		statMod(matrix, testValues);    														// Calls the statistical modeling numeric function    
	}


	// Calculates likelihoods (with Laplace estimator) and probabilities
	public void statMod(String[][] matrix, String[] testValues) {
		System.out.print("Statistical Modeling (Numeric):\nTest values: ");
		printArray(testValues);

		double likelihoodYes = likelihood(true, matrix, testValues);
		double likelihoodNo = likelihood(false, matrix, testValues);

		double probabilityYes = probability(true, likelihoodYes, likelihoodNo);
		double probabilityNo = probability(false, likelihoodYes, likelihoodNo);

		System.out.println("Likelihood 'yes' \t=\t" + likelihoodYes);
		System.out.println("Likelihood 'no' \t=\t" + likelihoodNo);
		System.out.println("Probability 'yes' \t=\t" + (Math.floor(probabilityYes * 10) / 10) + "%");
		System.out.println("Probability 'no' \t=\t" + (Math.floor(probabilityNo * 10) / 10) + "%\n");
	}


	// Calculates probability percentage
	public double probability(boolean yes, double likelihoodYes, double likelihoodNo) {
		double totalLikelihood = likelihoodYes + likelihoodNo;

		if (yes) {
			return (likelihoodYes / totalLikelihood) * 100;
		} else if (!yes) {
			return (likelihoodNo / totalLikelihood) * 100;
		} else {
			return 0;
		}
	}


	// Calculates likelihood
	public double likelihood(boolean yes, String[][] matrix, String[] testValues) {
		double result = 1;
		double yesTestValueCount = 0;

		double yesOrNoCount = yesOrNoCount(yes, matrix);										// Count of "yes" values in target column
		double totalCount = yesOrNoCount + yesOrNoCount(!yes, matrix);							// Total count of "yes" and "no" in target column

		double laplaceNumerator = 1;
		double laplaceDenominator = matrix[0].length - numberOfNumericValues(testValues);		// Only for the non-numeric (nominal) test values

		for (int index = 0; index < (matrix[0].length - 1); index++) {
			if (isNumeric(testValues[index])) {													// Check if test value is numeric
				result = result * probabilityDensityFunction(Double.parseDouble(testValues[index]), yesOrNoMean(yes, index, matrix), yesOrNoSd(yes, index, matrix));
			} else if (!isNumeric(testValues[index])) {
				yesTestValueCount = yesOrNoTestValueCount(yes, testValues[index], index, matrix);	// Count of tested value that equals "yes" in the target column
				result = result * ((yesTestValueCount + laplaceNumerator) / (yesOrNoCount + laplaceDenominator));	// Multiply all probabilities
			}
		}
		result = result * ((yesOrNoCount + laplaceNumerator) / (totalCount + laplaceDenominator));	// Multiply with target column probability
		return result;
	}
	
	
	// The probability density function
	public double probabilityDensityFunction(double x, double mean, double sd) {
		return (1 / ((Math.sqrt(2 * Math.PI)) * sd)) * Math.pow(Math.E, (-1 * (Math.pow((x - mean), 2) / (2 * Math.pow(sd, 2)))));
	}
	
	
	// Calculate the mean from a numeric column
	public double yesOrNoMean(boolean yes, int columnNumber, String[][] matrix) {
		double count = 0;
		double total = 0;
		double mean = 0;
		int targetColumn = matrix[0].length - 1;
		String compareString = "";
		
		if (yes) {																				// Check if "yes"- or "no"-count is requested
			compareString = "yes|Yes|YES";
		} else if (!yes) {
			compareString = "no|No|NO";
		}
		
		for (int index = 1; index < matrix.length; index++) {
			if (matrix[index][targetColumn].matches(compareString)) {							// Calculate variables to calculate mean
				try {
					total = total + Double.parseDouble(matrix[index][columnNumber]);
					count++;
				} catch (NumberFormatException e)	{
					continue;
				}
			}
		}
		
		mean = total / count;
		return mean;
	}
	
	
	// Calculate the standard deviation from a numeric column
	public double yesOrNoSd(boolean yes, int columnNumber, String[][] matrix) {
		double sd = 0;
		double mean = yesOrNoMean(yes, columnNumber, matrix);
		double total = 0;
		double count = 0;
		int targetColumn = matrix[0].length - 1;
		String compareString = "";
		
		if (yes) {																				// Check if "yes"- or "no"-count is requested
			compareString = "yes|Yes|YES";
		} else if (!yes) {
			compareString = "no|No|NO";
		}
		
		for (int index = 1; index < matrix.length; index++) {
			if (matrix[index][targetColumn].matches(compareString)) {							// Calculate variables to calculate variance
				try {
					total = total + (Math.pow((Double.parseDouble(matrix[index][columnNumber]) - mean), 2));
					count ++;
				} catch (NumberFormatException e)	{
					continue;
				}
			}
		}
		
		double variance = total / (count - 1);
		sd = Math.sqrt(variance);
		
		return sd;
	}


	// Counts occurrences of "yes" and "no" in the target column, for a specific test value
	public double yesOrNoTestValueCount(boolean yes, String testValue, int columnNumber, String[][] matrix) {
		double count = 0;
		String compareString = "";
		int targetColumn = matrix[0].length - 1;

		if (yes) {																				// Check if "yes"- or "no"-count is requested
			compareString = "yes|Yes|YES";
		} else if (!yes) {
			compareString = "no|No|NO";
		}

		for (int index = 1; index < matrix.length; index++) {									// Checks if test value is is "yes" or "no" and counts how often
			if (matrix[index][columnNumber].equals(testValue) && matrix[index][targetColumn].matches(compareString)) {
				count++;
			}
		}
		return count;
	}


	// Counts occurrences of "yes" and "no" in the target column
	public double yesOrNoCount(boolean yes, String[][] matrix) {
		double count = 0;
		String compareString = "";
		int targetColumn = matrix[0].length - 1;

		if (yes) {																				// Check if "yes"- or "no"-count is requested
			compareString = "yes|Yes|YES";
		} else if (!yes) {
			compareString = "no|No|NO";
		}

		for (int index2 = 1; index2 < matrix.length; index2++) {								// Loop through target column
			if (matrix[index2][targetColumn].matches(compareString)) {							// Add 1 to count if index is equal to value in compare String
				count++;
			}
		}
		return count;
	}
	
	
	// Counts the number of numeric values within the test values
	public double numberOfNumericValues(String[] testValues) {
		double count = 0;
		
		for (int index = 0; index < testValues.length; index++) {
			if (isNumeric(testValues[index])) {
				count++;
			}
		}
		return count;
	}
	
	
	// Checks if a String value is numeric
	public boolean isNumeric(String value) {
		try {
			Double.parseDouble(value);
		} catch (NumberFormatException e)	{
			return false;
		}
		return true;
	}


	// Turn list into String[][] matrix
	public String[][] listToMatrix(List<String> list, int columnCount) {
		int rowCount = list.size() / columnCount;
		String[][] matrix = new String[rowCount][columnCount];

		int count = 0;
		for (int row = 0; row < rowCount; row++) {
			for (int column = 0; column < columnCount; column++) {
				matrix[row][column] = list.get(count);
				count++;
			}
		}
		return matrix;
	}


	// Print array
	public void printArray(Object[] array) {
		for (int index = 0; index < array.length; index++) {
			System.out.print(array[index] + " ");
		}
		System.out.println();
	}
}