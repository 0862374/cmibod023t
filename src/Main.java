import java.io.FileNotFoundException;

public class Main {

	public static void main(String[] args) throws FileNotFoundException {
		
		//RobZeroR zeror = new RobZeroR();
		//zeror.zeroR("src/restaurant_clean.csv", "wait");		// Parameters to specify file location and target column name
		
		//RobOneR oner = new RobOneR();
		//oner.oneR("src/restaurant_clean.csv", "wait");			// Parameters to specify file location and target column name
		//System.out.println();
		
		//ZeroR zero = new ZeroR("src/restaurant_clean.csv", true);
		//zero.printFrequention();
		
		//new OneR("src/restaurant_clean.csv").getLowestErrors();
		
		//new StatisticalModeling("src/restaurant_clean.csv").createScreen();
		//new DecisionTree("src/restaurant_clean.csv");
		new DecisionTree("src/test.csv");
	}
}
